*************************************************
*                   Star Wars                   *
*                     V 1.0                     *
*                                               *
*                 Entwickelt von:               *
*                Omorov Beksultan               *
*                Anarbaev Elmurat               *
*                 Atai Algojoev                 *
*                                               *
*             Erstellt     April 2-14           *
*************************************************

1. über das Spiel:

Star Wars - Abenteuerspiel, basierend auf den Filmen "Star Wars". Das Spiel findet in unserer Galaxie "Milchstraße" statt, in unserem Sonnensystem (da der Lehrer uns 10 Räume bauen lässt). Es gibt 10 Planeten, Monster und verschiedene Geräte für den Spieler.

1.1 Monster:

In den Star Wars sind drei Monster versteckt

**********************
*Name       * Health *
**********************
**********************
*Darth Vader*   10   *
**********************
*Dart Mol   *    8   *
**********************
*Bob Fet    *    3   *
**********************

1.2 Dinge:
Star Wars, gibt es drei Dinge, die in drei separaten Räumen in der Übersicht versteckt sind. Alle drei halten absolut keinen Zweck. Jeder Gegenstand hat ein Gewicht. Sie können das maximale Gewicht von 1000 Einheiten nicht überschreiten.

**********************
* Name * Gewicht *
**********************
**********************
* Pflanze * 2.0 *
**********************
* Schwert * 7.0 *
**********************
* Pogo Stick * 5.0 *
**********************
1.3 Karte:

Das Star Wars besteht aus 10 Zimmern. Sie sind wie folgt zugeordnet.
(Wenn die Karte nicht sichtbar ist, Sie können im "Readme.md" hineinschauen

                                            *******************
                                            *                 *
                                            *                 *
                                            *                 *
                                            *       ERDE      *
                                            *                 *
                                            *                 *
                                            *                 *
                                            *                 *
                                            ********   ********
                                                   *   *
                                                   *   *
                      *******************   ********   ********   *******************
                      *                 *   *                 *   *                 *
                      *                 *   *                 *   *                 *
                      *                 *****                 *****                 *
                      *     Merkur                 Venus                  Mars      *
                      *                                                             *
                      *                 *****                 *****                 *
                      *                 *   *                 *   *                 *
                      *                 *   *                 *   *                 *
                      *******************   ********   ********   ********   ********
                                                   *   *                 *   *
                                                   *   *                 *   *
                      *******************   ********   ********   ********   ********
                      *                 *   *                 *   *                 *
                      *                 *   *                 *   *                 *
                      *                 *   *                 *****                 *
                      *    Jupiter      *   *      Saturn                Uranus     *
                      *                 *   *                                       *
                      *                 *   *                 *****                 *
                      *                 *   *                 *   *                 *
                      *                 *   *                 *   *                 *
                      ********   ********   *******************   ********   ********
                             *   *                                       *   *
                             *   *                                       *   *
                      ********   ********   *******************   ********   ********
                      *                 *   *                 *   *                 *
                      *                 *   *                 *   *                 *
                      *                 *****                 *****                 *
                      *      Neptun                Pluto                 Cerera     *
                      *                                                             *
                      *                 *****                 *****                 *
                      *                 *   *                 *   *                 *
                      *                 *   *                 *   *                 *
                      *******************   *******************   *******************